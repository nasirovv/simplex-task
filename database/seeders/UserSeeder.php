<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Bahodir',
            'email' => 'bahodir8586@gmail.com',
            'password' => 'bahodir123',
            'gender' => 'male',
            'company_name' => 'Unity',
            'address' => 'Bukhara',
        ]);

        DB::table('users')->insert([
            'name' => 'Asadbek',
            'email' => 'asadbek8586@gmail.com',
            'password' => 'asadbek123',
            'gender' => 'male',
            'company_name' => 'Unity',
            'address' => 'Bukhara',
        ]);
    }
}
