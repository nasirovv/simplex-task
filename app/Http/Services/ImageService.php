<?php

namespace App\Http\Services;

class ImageService{

    public function upload($request, $catalog, $id): string
    {
        $file = $request->file('photo');
        $fileName = time() . $file->getClientOriginalName();
        $filePath = "/storage/images/{$catalog}/{$id}/{$fileName}";
        $file->storeAs("public/images/{$catalog}/{$id}", $fileName);

        return $filePath;
    }

    public function update($request, $catalog, $id, $oldImagePath): string
    {
            $image_path = public_path(). '/' . $oldImagePath;
            unlink($image_path);

            return $this->upload($request, $catalog, $id);
    }
}
