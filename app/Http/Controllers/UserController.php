<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Http\Resources\UserResource;
use App\Http\Services\ImageService;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    private $catalog = 'users';

    public function index(): AnonymousResourceCollection
    {
        return UserResource::collection(User::all());
    }

    public function store(UserRequest $request): JsonResponse
    {
        DB::beginTransaction();
        try {
            $user = User::query()->create($request->except('photo'));
            if ($request->hasFile('photo')){
                $filePath = (new ImageService())->upload($request, $this->catalog, $user->id);
                $user->fill(['photo' => $filePath]);
                $user->save();
            }
        } catch (\Exception $e){
            Log::alert("Unable to upload image. {$e->getMessage()}");
            DB::rollBack();
            return response()->json('Something went wrong', 400);
        }
        DB::commit();
        return response()->json('Successfully created', 201);
    }

    public function show(User $user): UserResource
    {
        return new UserResource($user);
    }

    public function update(UserRequest $request, User $user): JsonResponse
    {
        DB::beginTransaction();
        try{
            $user->update($request->except('photo'));
            if ($request->hasFile('photo')) {
                $filePath = (new ImageService())->update($request, $this->catalog, $user->id, $user->photo);
                $user->fill(['photo' => $filePath]);
                $user->save();
            }
        }catch (\Exception $e){
            Log::alert("Unable to upload image. {$e->getMessage()}");
            DB::rollBack();
            return response()->json('Something went wrong', 400);
        }
        DB::commit();
        return response()->json('Successfully updated', 201);
    }

    public function destroy(User $user): JsonResponse
    {
        $user->delete();
        return response()->json('User deleted successfully', 200);
    }
}

