<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|string',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:6',
            'gender' => 'required|in:male,female',
            'company_name' => 'required|string',
            'address' => 'required|string',
            'photo' => 'mimes:jpeg,jpg,png,gif|nullable|max:10000'
        ];
        if ($this->routeIs('users.update')){
            $rules['email'] = 'required';
        }

        return $rules;
    }
}
